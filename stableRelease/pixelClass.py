class pixelObject ():

    def __init__(self, rgbTuple, xyTuple=(-1,-1)):
        self.rgb = rgbTuple
        self.xy = xyTuple
        self.cluster = -1
    def setCluster(self, i):
        self.cluster = i
    def getCluster(self):
        return self.cluster
    def getRGB(self):
        return self.rgb
    def getXY(self):
        return self.xy
