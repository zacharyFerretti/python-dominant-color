from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.metrics import accuracy_score
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import mean_absolute_error
import pandas
import csv

import matplotlib.pyplot as pt
import numpy as nump
import math
from scipy.interpolate import make_interp_spline, BSpline


# Zachary Ferretti
# CS 589 - Homework 3
# October 29th, 2019





# Function I created to calculate the cross validated Mean Absolute Erorr (MAE) of the model (used here by the regression
#   functions) and the corresponding training data. I use 5 fold cross validation to calculate these scores.
def crossValidateMAE(theModel, xTrain, yTrain):
    '''totalError = 0
    totalPredictions = 0
    for i in range(0, 5):
        newTestX = []
        newTestY = []
        newTrainX = []
        newTrainY = []
        # Splits the training data into 4/5 of data which is then tested on the other 1/5 that is remaining. The way that
        #   I ensure there is different test data being tested on each time, is that by checking if the modulo of J equals I,
        #   this will see if it is 0, 1, 2, 3, 4 thus ensuring each execution of the loop with i has a different fifth being tested
        #   on.
        xTrainList = xTrain.values.tolist()
        yTrainList = yTrain.values.tolist()
        for j in range(len(xTrain)):
            if j % 5 == i:
                newTestX.append(xTrainList[j])
                newTestY.append(yTrainList[j])
            else:
                newTrainX.append(xTrainList[j])
                newTrainY.append(yTrainList[j])

        # After I have fitted the model on the 4/5 of training data, I test it on the other fifth, and then
        #   use our predictions to calculate the mean absolute error of each iteration (absoluteSum/len(predictions)
        #   and add this to the running total error while incrimenting total predictions by one. I then return
        #   totalError/totalPredictions to get the averaged MAE.

        theModel.fit(newTrainX, nump.ravel(newTrainY, order='C'))
        predictions = theModel.predict(newTrainX)
        #absoluteSum = 0
        #for i in range(len(predictions)):
        #    absoluteSum = absoluteSum + abs(newTrainY[i] - predictions[i])
        error = mean_absolute_error(newTrainY, predictions)
        totalError = totalError + error
        totalPredictions = totalPredictions + 1

    return (totalError / totalPredictions)
    '''
    theModel.fit(xTrain, yTrain)
    test = [[]]
    test.append([143, 255, 173])
    print(theModel.predict(test))



# Function for linear regression.
def linearRegression(xTrain, yTrain):
    regressor = LinearRegression(fit_intercept=True, normalize=False, copy_X=True, n_jobs=None)
    oOSMAE = crossValidateMAE(regressor, xTrain, yTrain)
    





def main():
    allTraining = pandas.read_csv('/Users/zach/code/rgb_two_dominant.csv', sep="|", header=None)
    #print(allTraining[0])
    linearRegression(allTraining[0], allTraining[1])
    
    


if __name__ == "__main__":
    main()
