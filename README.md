# Python Dominant Color

_Note: At the moment, if you run the code you will not see any output generated as I have turned it off. It saves output to a CSV file. I plan on changing this back to the original functionality soon._

### TO-DO

- [x] ~~Have output formatted properly in one window.~~
- [ ] Show which pixels belong to each cluster.
- [ ] Work on informatics display.
- [ ] Consider app possibilities.
- [x] ~~Support recursive search of folders, and save output to a CSV file~~

### ABOUT

This is my project to determine the dominant color in a photograph. This is the
first step towards setting up program to sort photographs by color. This program works by using K-Means clustering to determine which colors are dominant in an image. By selecting _n_ colors in the image we start the process of clustering, determining which cluster (color) the rest of the points belong to. Then, I compute the average color of this cluster, re center the pixels, and repeat this process until the change in distance between the center movement is less than ten.

- The folder _stableRelease_ houses the .py file which is currently working and
  stable. This file will be updated as time goes on to reflect the updates I bring to this program.

- The folder _inProgress_ is where I will be performing programming and updating on the project as time goes on. And it may have bugs or issues.

### LOOKING FORWARD

I have a few things in mind for this project that I would like to accomplish before I consider it really finished.

- ~~Implement multiple runs of the algorithm, who's results will then be stored
  and combined together to determine a more accurate representation of the true
  dominant colors.~~ ~~_I have decided this would not be worth pursuing. Instead I
  increased thumbnail size, and reduced the threshold for what was considered accurate.
  This achieves much better results._~~ _I take back what I said above, I plan on doing this at some point in the future not sure when though._
- ~~I would also like to set it up so that the colors which are outputted to the
  screen are all stored in one singular screen. As well as display the photo we are examining.~~ June 20th

- ~~The final feature which I really want to implement or give thought to is
  counting up the pixels which correspond to being similar enough to each cluster
  to count, and then determine with that which would be the true dominant color.~~
  ~~_The route, I am going to take is to instead determine which cluster has the highest count,
  and refer to this as the dominant color._~~ _Finished this the week of January 18th, the colors that are displayed are sorted least to most dominant._


Once these features are implemented I will feel solid about this project, and move
onto the next project, which will be using this algorithm to help sort photos by color.

I have begun working on this project again (Jan-10-2020) and I intend to leverage it to create a different neural network model using the outputs of this model over images of mine in order to generate a model which given an input color can produce and recommend colors that will (most likely) be a similar palette and complimentary. To do that the next steps I readily envision are

- Storing the output in a machien readable and retrievable format.
- Allowing the input to be run over a folder (no matter how much nesting occurs)
- Then work on neural networks.

### USAGE

This program has a few python dependencies that it is important to be considerate of if you want to run it on your local machine, to that end here is a list of the pip3 install statements you would need to use to prepare your system.

- `pip3 install scipy`
- `pip3 install pillow`
- `pip3 install numpy`
- `pip3 install imageio`
- `pip3 install matplotlib`

Once that is squared away, it is as simple as going into the _stableRelease_ folder running the command:
`python3 openPixel.py _yourPhoto.jpg_ _numberOfClusters_`
The higher number of clusters will have more colors displayed. I have found that five is usually a fairly accurate indication of the dominant colors in an image.

### SAMPLE OUTPUT

![Sample Output One](./inProgress/sampleOutput1.png)

_Sample of the newly formatted output_
![Sample Output One](./inProgress/Progress2.png)
