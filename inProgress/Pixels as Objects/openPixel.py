from PIL import Image
from pixelClass import pixelObject
from scipy import misc
import imageio, numpy as np, matplotlib.pyplot as plt, random, sys

def distanceCalculator(pxObj,clstPoint):
    return (((pxObj.getRGB()[0]-clstPoint.getRGB()[0])**2) + ((pxObj.getRGB()[1]-clstPoint.getRGB()[1])**2) + ((pxObj.getRGB()[2]-clstPoint.getRGB()[2])**2))**(1/2)


###These two functions together function takes a pixel object, and then determines
###     which cluster this object would best be suited as a part of. The bottom function
###     goes through the image representation, and calls clusterchooser for each pixelObject.
def clusterChooser(pxObj):
    global clusterPoints
    bestCluster = -1
    bestDistance = -1
    for i in range(len(clusterPoints)):
        clstPoint = clusterPoints[i]
        distance = distanceCalculator(pxObj,clstPoint)
        if (bestDistance == -1 or distance < bestDistance):
            bestDistance = distance
            bestCluster = i


    pxObj.setCluster(bestCluster)

def generateClusterPoints():
    global clusterPoints
    for i in range(len(imageRepresentation)):
        clusterChooser(imageRepresentation[i])


###This function will take in a PIL IMAGE file and converts it to a thumbnail
###     to allow for faster color swapping, I have it as 256 right now, but by
###     increasing the size, I have a feeling the accuracy would be better. But
###     right now this is plenty accurate color rendition.
def toThumbNail():
    thmbnailsize = 256, 256
    thePhoto.thumbnail(thmbnailsize)
    print("The number of pixels is: " + str(thePhoto.size[0] * thePhoto.size[1]))


###Creates a pixel object for each pixel of the thumbnail, and then chooses the
###     random K cluster centers to start off of.
def createImageRepresentation():
    global imageRepresentation, clusterPoints

    numRows, numCols = thePhoto.size
    imageRepresentation = []

    clusterPoints = []
    pixels = thePhoto.load()

    for i in range(numRows):
        for j in range(numCols):
            temp = pixelObject(pixels[i,j] , (i,j))
            imageRepresentation.append(temp)

    for x in range(int(sys.argv[2])):
        randomInt = random.randint(0,len(imageRepresentation))
        clusterPoints.append(imageRepresentation[randomInt])


###This function determines the new clusters, and then returns an array of the new
###     pixel objects that we create based off of the current clusters.
def createNewCenters():
    count = [0]*int(sys.argv[2])
    rgbasPixels = []*int(sys.argv[2])
    rgb = [[0,0,0]for i in range (int(sys.argv[2]))]
    global imageRepresentation

    for i in range (len(imageRepresentation)):
        currCluster = imageRepresentation[i].getCluster()
        currRGB = imageRepresentation[i].getRGB()
        count[currCluster]+=1
        rgb[currCluster][0]+=currRGB[0]
        rgb[currCluster][1]+=currRGB[1]
        rgb[currCluster][2]+=currRGB[2]
    for i in range (len(rgb)):
        rgb[i][0]=int(rgb[i][0]/count[i])
        rgb[i][1]=int(rgb[i][1]/count[i])
        rgb[i][2]=int(rgb[i][2]/count[i])
        rgbasPixels.append(pixelObject((rgb[i][0],rgb[i][1],rgb[i][2])))
    return rgbasPixels


###This function calls createNewCenters and stores those results as the newRGBPixels array.
###     If the distance between any of the old clusters and new points is greater than 10,
###     then clusterPoints becomes a copy of the newRGBPixels values and returns false. Otherwise
###     it will return true, and the program will exit.
def recenter():
    global clusterPoints
    newRGBPixels = createNewCenters()
    for i in range (int(sys.argv[2])):
        if (distanceCalculator(clusterPoints[i],newRGBPixels[i])>3):
            clusterPoints.clear()
            clusterPoints = newRGBPixels.copy()
            return False
    return True


###This function displays the output with all of the colors and the image all in one window,
###     the next step for me to implement here will be making it so that it can print different
###     aspect ratios, properly.
def displayOutput():
    figures = []
    for i in range(int(sys.argv[2])):
        #print (clusterPoints[i].getRGB())
        figures.append(Image.new("RGB", (200,200), clusterPoints[i].getRGB()))
    x,y = figures[0].size
    ncol = 4
    nrow = 2
    tuple = x*ncol,x*nrow
    ourImage = Image.open(sys.argv[1])
    ourImage.thumbnail(tuple)
    thumbX, thumbY = ourImage.size
    allInOne = Image.new('RGB', (x*ncol+thumbX,x*nrow))
    for i in range (len(figures)):
        px,py = x*int(i/nrow), y*(i%nrow)
        allInOne.paste(figures[i],(px,py))
    allInOne.paste(ourImage,(x*4,0))
    allInOne.show()


###This is the grand k-means function which calls each of the other functions in
###     this file, and is responsible for running the loop as many times as it must
###     before the accuracy I have deemed enough is reached.
def kMeans():
    global thePhoto
    thePhoto = Image.open(sys.argv[1])
    hasConverged = False
    toThumbNail()
    createImageRepresentation()

    while(hasConverged==False):
        generateClusterPoints()
        hasConverged=recenter()
    displayOutput()

def main():
    kMeans()
if __name__ == "__main__":
    main()
