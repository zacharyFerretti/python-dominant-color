import random

from PIL import Image


#To pick back up, I need to work on recalculateCenter.  IHAVE ACHIEVED CONVERGENCE.
# next step is to figure out which of the finalized dominant colors had the largest count,
# THIS is the dominant color. I also want to make it so all the colors are organized in one output not multiple.

def toThumbNail(thePhoto):
    #This function will take in a PIL IMAGE file and then return the thumbnail of it for faster
    # cluster sampling.
    thmbnailsize = 256, 256
    thePhoto.thumbnail(thmbnailsize)
    print("The number of pixels is: " + str(thePhoto.size[0] * thePhoto.size[1]))

def createArrays(thePhoto, n):
    #This function will create the pixels array which holds the colors of the image
    width, height=thePhoto.size
    #Declares the global arrays, for the array of distributed values, as well as
    # the array of all the color values.
    global pixelsOriginal, pixelsCopy, distributedArray, originalRGB
    pixelsOriginal = thePhoto.getcolors(width*height)
    pixelsCopy = pixelsOriginal.copy()
    distributedArray = [[] for i in range(n)]
    originalRGB = []

    for i in range(n):
        distributedArray[i].append(pixelsCopy.pop(random.randint(0,len(pixelsCopy))))
        originalRGB.append(distributedArray[i][0][1])

def distanceCalculator(rgbValuesOne, rgbValuesTwo):
    #Function to calculate distance between two sets of RGB points which I am treating
    # as if they were points in three dimensional space.
    # The equation is: [ (r2-r1)^2 + (g2-g1)^2 + (b2-b1)^2 ] ^ (1/2)
    return (((rgbValuesTwo[0]-rgbValuesOne[0])**2) + ((rgbValuesTwo[1]-rgbValuesOne[1])**2) + ((rgbValuesTwo[2]-rgbValuesOne[2])**2))**(1/2)

def pointChooser(rgbValues):
    #This function calculates the centroid cluster for which each color record
    # belongs to, and returns this index.
    closest = -1
    bestDistance = -1
    for i in range(len(distributedArray)):
        distance = distanceCalculator(rgbValues, distributedArray[i][0][1])
        if (bestDistance == -1 or distance<bestDistance):
            bestDistance = distance
            closest = i
    return closest

def recalculateCenter():
    #This function calculates the RGB value of the centroid. This is not trivial,
    # solely because each entry is not just corresponding to ONE appearance of (R,G,B)
    # each entry is (Count, (R,G,B)) so for each cluster, while doing this I will need to do a few things.
    # I will have to keep track of the count of each one (totalCount=totalCount+entry[0][0] <-- This is the
    # count for each entry) and I will also need to when adding to the R G or B value, add Count * R or
    # Count * G etc, etc. This way the number of appearances of the pixels are weighed appropriately.
    # I will then divide those scores by the
    # computedTuple = 1,(compR, compG, compB)
    # When adding the newly computed center value, use distributedArray[cluster].insert(0, (1, (computedR, computedG, computedB)))
    global pixelsCopy,pixelsOriginal
    for i in range(len(distributedArray)):
        rSum = 0
        gSum = 0
        bSum = 0
        countOfCluster=0
        #print("\n\nAt index I the length is originally: " + str(len(distributedArray[i])))
        for j in range(len(distributedArray[i])):
            countOfCluster=countOfCluster+distributedArray[i][j][0]
            #Sum = sum + countOfChannel*valueOfChannel
            rSum=rSum+distributedArray[i][j][0]*distributedArray[i][j][1][0]
            gSum=gSum+distributedArray[i][j][0]*distributedArray[i][j][1][1]
            bSum=bSum+distributedArray[i][j][0]*distributedArray[i][j][1][2]
        newR = int(float(rSum) / float(countOfCluster))
        newG = int(float(gSum) / float(countOfCluster))
        newB = int(float(bSum) / float(countOfCluster))
        distributedArray[i].clear()
        distributedArray[i].append((1,(newR,newG,newB)))
        #print("At index I the length is now: " + str(len(distributedArray[i])))
        #print("With contents:" + str(distributedArray[i][0]))
        #print("This cluster had a count of: " + str(countOfCluster))
    pixelsCopy.clear()
    pixelsCopy = pixelsOriginal.copy()

def centerPoints():
    #This function appends the color value to the appropriate centroid.
    while pixelsCopy:
        closest = pointChooser(pixelsCopy[0][1])
        distributedArray[closest].append(pixelsCopy.pop(0))

def hasNotConvergedYet():
    global hasNotConverged, originalRGB
    distance = []
    for i in range(len(distributedArray)):
        distance.append(distanceCalculator(originalRGB[i], distributedArray[i][0][1]))
    for i in range(len(distance)):
        if distance[i]>10:
            hasNotConverged=True
            originalRGB.clear()
            for i in range(len(distributedArray)):
                originalRGB.append(distributedArray[i][0][1])
            break
    hasNotConverged=False

def kMeans(image, n):
    #This is the grand K means function, which calls in order the different steps
    # of a k-means clustering program.
    global distributedArray
    global hasNotConverged
    hasNotConverged=True
    toThumbNail(image)
    createArrays(image, n)
    #for i in range(n):
    #    print("Length of " +str(i)+" distribution: " + str(len(distributedArray[i])))
    while (hasNotConverged==True):
        centerPoints()
        recalculateCenter()
        hasNotConvergedYet()
    for i in range(len(distributedArray)):
        image= Image.new("RGB", (200,200), distributedArray[i][0][1])
        image.show()

def displayCentroid():
    # This function will go through the image and the distributed array and determine
    #   for each pixel, what RGB value those pixels correspond to. There are two ideas on how this
    #   could be done. It would be faster if we just compare each pixel color to the final center rgbValues
    #   at distributedArray[i][0][1]. Or we could go through each pixel in that cluster and see which one it is in.
    pass